import pytest
from app import create_app

@pytest.fixture()
def app():
    app = create_app()
    app.testing = True
    yield app

@pytest.fixture()
def client(app):
    return app.test_client()

def test_index_returns_200(client, app):
    resp = client.get('/')
    assert resp.status_code == 200

def test_index_returns_hello_text(client, app):
    resp = client.get('/')
    assert b'hello' in resp.data