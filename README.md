# Exercise to create a route using Flask

The unit tests will fail on this project, see the files in `app/test`.  Try to update the project to get the unit tests to pass w/o changing the unit tests themselves.

> *NOTE* This project assumes that you have Python 3.8+ installed.

## Setup

1. Get virtualenv setup
    ```
    python -m venv venv
    ```

2. Run it!
    ```
    flask run
    ```

3. Test it!
    ```
    pytest
    ```